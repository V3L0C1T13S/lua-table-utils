# Lua Table Utils

A helper package for Lua tables, adding utilities to avoid some of the nonsense that rbxts can give you.

# Example usage
```typescript
import { Unpacker } from "@rbxts/lua-table-utils";

export class AssaultRifleServer extends Mixin(BaseWeaponServer, AssaultRifle) {
    constructor(...args: unknown[]) {
        /** Normally this would error out with ...args, but won't with Unpack. */
		super(Unpacker.Unpack(args));
		print("Success");
	}
}

```