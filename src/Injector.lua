local Injector = {}
Injector.__index = Injector

function Injector.Prefix(method, callback)
    local newFunc = function(...)
        local newRet = callback(method, ...)
        if newRet == nil then
            return method(...)
        else
            return unpack(newRet)
        end
    end
    -- Inject into the method
    method = newFunc;
    return method;
end

function Injector.Postfix(method, callback)
    local newFunc = function(...)
        local ret = {method(...)}
        local newRet = callback(unpack(ret))
        return unpack(newRet)
    end
    -- Inject into the method
    method = newFunc;
    return method;
end

return Injector