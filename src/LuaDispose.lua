local LuaDispose = {}
LuaDispose.__index = LuaDispose

--[[
    Disposes of a lua object from memory.
]]
function LuaDispose.dispose(obj)
    obj.__mode = "kv"
    obj = nil
end

return LuaDispose