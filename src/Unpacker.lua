local Unpacker = {}
Unpacker.__index = Unpacker

--[[
    Unpacks the given tables into a multiple values.
    Useful for when Typescript gives you garbage about spread args.
--]]
function Unpacker.Unpack(...)
    local ret = {}
    for i,v in pairs(...) do
        table.insert(ret, v)
    end
    return unpack(ret)
end


return Unpacker