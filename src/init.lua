local TableUtils = {}

-- Version check for Roblox Luau.
if _VERSION == "Luau" then
    for i,v in pairs(script:GetDescendants()) do
        if v:IsA("ModuleScript") then
            TableUtils[v.Name] = require(v);
        end
    end
else
    TableUtils = {
        LuaDispose = require('LuaDispose'),
        Unpacker = require('Unpacker'),
        Injector = require('Injector'),
    }
end

return TableUtils