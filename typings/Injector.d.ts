interface Injector {
	Prefix: <T>(method: Callback, callback: Callback) => T;
	Postfix: <T>(method: Callback, callback: Callback) => T;
}

declare const Injector: Injector;
export = Injector;
