interface LuaDispose {
	/** Disposes of a lua object from memory. */
	dispose(obj: any): void;
}

declare const LuaDispose: LuaDispose;
export = LuaDispose;
