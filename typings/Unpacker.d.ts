interface Unpacker {
	/** Unpacks the given tables into a multiple values. */
	Unpack: <T>(...args: any[]) => T;
}

declare const Unpacker: Unpacker;
export = Unpacker;
